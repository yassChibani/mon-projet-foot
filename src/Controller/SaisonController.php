<?php

namespace App\Controller;

use App\Entity\Saisoon;
use App\Repository\SaisonnRepository;
use App\Repository\SaisoonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SaisonController extends AbstractController
{
    //cette méthode nous permet de recuperer toutes les saisons disponibles

    /**
     * @Route("/listesaison", name="saison")
     *
     */
    public function index(SaisoonRepository $saisonRepository ) :Response
    {
        $saison = $saisonRepository->findAll();
        dd($saison);
        return $this->render('saison/index.html.twig', [
            'controller_name' => 'SaisonController',
        ]);
    }
}
