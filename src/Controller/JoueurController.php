<?php

namespace App\Controller;

use App\Entity\Joueur;
use App\Repository\HistoriqueRepository;
use App\Repository\JoueurRepository;
use App\Repository\SaisoonRepository;
use DateInterval;
use DatePeriod;
use Doctrine\ORM\EntityManager;
use Faker\Factory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Controller\Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;


class JoueurController extends AbstractController
{
    /**
     * @Route("/joueur/{id}", name="joueur", methods={"GET"},defaults={"id"=null})
     */
    public function index(JoueurRepository $joueurRepository, $id=null): Response
    {

        //Vérifier si on a un filtre par joueur sinon interroger ka base pour avoir tout les joueurs
        if($id){
            $joueurs = $joueurRepository->findByClub($id);
        }else{
            $joueurs = $joueurRepository->findAll();
        }

        return $this->render('joueur/index.html.twig', [
            'joueurs' => $joueurs
        ]);
    }

    // Methode pour Récupérer les statistiques par joueur

    /**
     * @Route("/statistique/{id}", name="statistique", methods={"GET"},defaults={"id"=null})
     */
    public function statistiqueAction(HistoriqueRepository $historiqueRepository, $id=null): Response
    {

        if($id){
            $stats = $historiqueRepository->findStats($id);

        }else{
            $stats = $historiqueRepository->findAll();
        }

        return $this->render('joueur/stats.html.twig', [
            'stats' => $stats
        ]);
    }
}
