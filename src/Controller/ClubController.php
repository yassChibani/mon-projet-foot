<?php

namespace App\Controller;

use App\Repository\ClubRepository;
use App\Repository\HistoriqueRepository;
use App\Repository\SaisoonRepository;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ClubController extends AbstractController
{

    // Methode pour Récupérer toute la liste des équipes
    /**
     * @Route("/", name="club")
     */
    public function listeDesClubs(ClubRepository $clubRepository, SaisoonRepository $saisonRepository)
    {
        $saisons = $saisonRepository->findAll();
        $clubs = $clubRepository->findAll();
        return $this->render('club/index.html.twig', [
            'clubs' => $clubs,
            'saisons' => $saisons,
        ]);
    }

    // Methode pour Récupérer toute la liste des équipes par saison

    /**
     * @Route("/get_liste_club_by_saison", name="get_liste_club_by_saison")
     */
    public function listeDesClubsBySaison(Request $request, ClubRepository $clubRepository) :Response
    {


            $saison_id= $request->query->get("clientSelector");

             $clubs = $clubRepository->findListeClubBySaison($saison_id);

             return $this->render('club/liste.html.twig', [
            'clubs' => $clubs,

        ]);




    }
}

