<?php

namespace App\Entity;

use App\Repository\SaisoonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SaisoonRepository::class)
 */
class Saisoon
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $annedebut;

    /**
     * @ORM\Column(type="integer")
     */
    private $annefin;

    /**
     * @ORM\OneToMany(targetEntity=Historique::class, mappedBy="saisoon")
     */
    private $historiques;

    public function __construct()
    {
        $this->historiques = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAnnedebut(): ?int
    {
        return $this->annedebut;
    }

    public function setAnnedebut(int $annedebut): self
    {
        $this->annedebut = $annedebut;

        return $this;
    }

    public function getAnnefin(): ?int
    {
        return $this->annefin;
    }

    public function setAnnefin(int $annefin): self
    {
        $this->annefin = $annefin;

        return $this;
    }

    /**
     * @return Collection|Historique[]
     */
    public function getHistorique(): Collection
    {
        return $this->historiques;
    }

    public function addHistorique(Historique $historique): self
    {
        if (!$this->historiques->contains($historique)) {
            $this->historiques[] = $historique;
            $historique->setSaisoon($this);
        }

        return $this;
    }

    public function removeHistorique(Historique $historique): self
    {
        if ($this->historiques->contains($historique)) {
            $this->historiques->removeElement($historique);
            // set the owning side to null (unless already changed)
            if ($historique->getSaisoon() === $this) {
                $historique->setSaisoon(null);
            }
        }

        return $this;
    }

    public  function  __toString()
    {
        return $this->getAnnedebut().'/'.$this->getAnnefin();
    }
}
