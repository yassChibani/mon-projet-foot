<?php

namespace App\Entity;

use App\Repository\HistoriqueRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HistoriqueRepository::class)
 */
class Historique{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Joueur::class, inversedBy="historiques")
     */
    private $joueur;

    /**
     * @ORM\ManyToOne(targetEntity=Club::class, inversedBy="historiques")
     */
    private $club;



    /**
     * @ORM\Column(type="integer")
     */
    private $but;

    /**
     * @ORM\ManyToOne(targetEntity=Saisoon::class, inversedBy="historique")
     */
    private $saisoon;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getJoueur(): ?Joueur
    {
        return $this->joueur;
    }

    public function setJoueur(?Joueur $joueur): self
    {
        $this->joueur = $joueur;

        return $this;
    }

    public function getClub(): ?Club
    {
        return $this->club;
    }

    public function setClub(?Club $club): self
    {
        $this->club = $club;

        return $this;
    }





    public function getBut(): ?int
    {
        return $this->but;
    }

    public function setBut(int $but): self
    {
        $this->but = $but;

        return $this;
    }

    public function getSaisoon(): ?Saisoon
    {
        return $this->saisoon;
    }

    public function setSaisoon(?Saisoon $saisoon): self
    {
        $this->saisoon = $saisoon;

        return $this;
    }
}
