<?php

namespace App\DataFixtures;

use App\Entity\Club;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ClubFixtures extends Fixture implements  OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {


        $faker = Factory::create('fr_FR');
        for($i = 0; $i < 20; $i++){
            $club = new Club();
            //$this->addReference('Victoire', $joueur);
            $this->setReference('club.id'.$i, $club);
            $club->setNom($faker->name);





            $manager->persist($club);
        }

        $manager->flush();
    }
    public function getOrder()
    {
        return 3;
    }
}
