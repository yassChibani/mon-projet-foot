<?php

namespace App\DataFixtures;

use App\Entity\Joueur;
use App\Entity\Saisoon;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class SaisonFixtures extends Fixture implements  OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        $now = new \DateTime();



        $dd = $now->format('Y');
        $df = $now->modify('+1 year');
        $tab=[];
        $tab1=[];
        $satrtyaer =0;
        $endyaer =0;
        for($i=2000;$i<2006;$i++)
        {
            $satrtyaer=$i;
            $endyaer=$satrtyaer+1;
            array_push($tab,$satrtyaer);
            array_push($tab1,$endyaer);

        }

        for($i = 0; $i < count($tab1); $i++){
            $saison= new Saisoon();
            $this->setReference('saison.id'.$i, $saison);
            $saison->setAnnedebut($tab[$i]);
            $saison->setAnnefin($tab1[$i]);
            $manager->persist($saison);
        }

        $manager->flush();
    }
    public function getOrder()
    {
        return 2;
    }
}
