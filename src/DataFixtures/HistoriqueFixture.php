<?php

namespace App\DataFixtures;

use App\Entity\Historique;
use App\Entity\Joueur;
use App\Repository\JoueurRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class HistoriqueFixture extends Fixture implements  OrderedFixtureInterface
{


    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');





        // Pour chaque Joueur on génére plusieurs historique,plusieurs saisons,plusieurs clubs
        for ($t=0; $t<6; $t++) {
            $joueur = $this->getReference('joueur.id'.$t);

            $saison = $this->getReference('saison.id' . $t);


            for ($i = 0; $i < mt_rand(1,5); $i++) {
                $club = $this->getReference('club.id' . $i);
                $historique = new Historique();
                $historique->setJoueur($joueur);
                $historique->setClub($club);
                $historique->setSaisoon($saison);
                $historique->setBut($faker->numberBetween(1, 50));
                $manager->persist($historique);
            }

        }







        $manager->flush();
    }

    public function getOrder()
    {
        return 5;
    }


}
