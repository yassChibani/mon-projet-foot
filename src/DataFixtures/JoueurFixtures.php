<?php

namespace App\DataFixtures;

use App\Entity\Historique;
use App\Entity\Joueur;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class JoueurFixtures extends Fixture implements  OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {


        $faker = Factory::create('fr_FR');
        for($i = 0; $i < 20; $i++){
            $joueur = new Joueur();
            //$this->addReference('Victoire', $joueur);
            $this->setReference('joueur.id'.$i, $joueur);
            $joueur->setNom($faker->name);
            $joueur->setPrenom($faker->firstName);
            $joueur->setNumero($faker->unique()->numberBetween(1,20));


            //$joueur->addHistorique($this->getReference(HistoriqueFixture::Historique_REFERENCE));

            /* $item = $em->getReference('MyProject\Model\Item', $itemId);
             $cart->addItem($item);
             $this->craeteMany(Historique::class, 5, function(Historique $historique){
                 $historique->setJoueur($this->getReference(Joueur::class.'_0'));
             });*/

            $manager->persist($joueur);
        }

        $manager->flush();
    }




    public function getOrder()
    {
        return 1;
    }
}
