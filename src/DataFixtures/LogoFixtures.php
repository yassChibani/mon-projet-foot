<?php

namespace App\DataFixtures;

use App\Entity\Historique;
use App\Entity\Joueur;
use App\Entity\Logo;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;

class LogoFixtures extends Fixture implements  OrderedFixtureInterface
{


    private $webDirectory;
    public function __construct($webDirectory)
    {
        $this->webDirectory = $webDirectory;
    }


        public function load(ObjectManager $manager)
        {
            $faker = Factory::create('fr_FR');

            $start = $faker->dateTimeBetween('-5 years', '+ 1years');
            $end = $faker->dateTimeBetween($start, $start->format('Y-m-d').' +1 years');




            // For each team, generate some players

            for ($t=0; $t<mt_rand(5,11); $t++) {
                $club = $this->getReference('club.id'.$t);

                for ($i = 0; $i <mt_rand(1,8); $i++) {

                    $logo = new Logo();
                    $logo->setClub($club);
                    $start = $faker->dateTimeBetween('-5 years', '+ 1years');
                    $end = $faker->dateTimeBetween($start, $start->format('Y-m-d').' +1 years');

                    $logo->setDatedebut($start);
                    $logo->setDatefin($end);


                    $manager->persist($logo);
                }

            }

            $manager->flush();
    }

    public function getOrder()
    {
        return 4;
    }

}
