<?php

namespace App\Repository;

use App\Entity\Club;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Club|null find($id, $lockMode = null, $lockVersion = null)
 * @method Club|null findOneBy(array $criteria, array $orderBy = null)
 * @method Club[]    findAll()
 * @method Club[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClubRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Club::class);
    }

    // Service pour récupérer touts les equipes par une saison donnée


    public function findListeClubBySaison($saisonid)
    {


        if($saisonid != null ) {


            return $this->createQueryBuilder('c')
                ->innerJoin('c.historiques', 'h')
                ->innerJoin('h.saisoon', 's')


                ->andWhere('s.id = :saisonid')
                ->setParameter('saisonid', $saisonid)
                ->GroupBy('c.id , s.id')
                ->getQuery()
                ->getResult()
                ;

        }
    }
}
