<?php

namespace App\Repository;

use App\Entity\Saisoon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\GroupBy;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Saisoon|null find($id, $lockMode = null, $lockVersion = null)
 * @method Saisoon|null findOneBy(array $criteria, array $orderBy = null)
 * @method Saisoon[]    findAll()
 * @method Saisoon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SaisoonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Saisoon::class);
    }

    // /**
    //  * @return Saisoon[] Returns an array of Saisoon objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Saisoon
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */


}
