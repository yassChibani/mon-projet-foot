<?php

namespace App\Repository;

use App\Entity\Joueur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Joueur|null find($id, $lockMode = null, $lockVersion = null)
 * @method Joueur|null findOneBy(array $criteria, array $orderBy = null)
 * @method Joueur[]    findAll()
 * @method Joueur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JoueurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Joueur::class);
    }

    // Service pour récupérer touts les joueurs par club

    public function findByClub($id)
    {
        $qb = $this->createQueryBuilder('j');
        return $qb->join('j.historiques', 'h')
            ->join('h.club', 'c')
            ->where('c.id = :val')
            ->setParameter('val', $id)
            ->getQuery()
            ->getResult();
    }

}
