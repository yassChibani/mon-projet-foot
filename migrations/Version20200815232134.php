<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200815232134 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE club (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE historique (id INT AUTO_INCREMENT NOT NULL, joueur_id INT DEFAULT NULL, club_id INT DEFAULT NULL, saisoon_id INT DEFAULT NULL, but INT NOT NULL, INDEX IDX_EDBFD5ECA9E2D76C (joueur_id), INDEX IDX_EDBFD5EC61190A32 (club_id), INDEX IDX_EDBFD5EC7F1732D (saisoon_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE joueur (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, numero INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE logo (id INT AUTO_INCREMENT NOT NULL, club_id INT DEFAULT NULL, datedebut DATE NOT NULL, datefin DATE NOT NULL, INDEX IDX_E48E9A1361190A32 (club_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE saisoon (id INT AUTO_INCREMENT NOT NULL, annedebut INT NOT NULL, annefin INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE historique ADD CONSTRAINT FK_EDBFD5ECA9E2D76C FOREIGN KEY (joueur_id) REFERENCES joueur (id)');
        $this->addSql('ALTER TABLE historique ADD CONSTRAINT FK_EDBFD5EC61190A32 FOREIGN KEY (club_id) REFERENCES club (id)');
        $this->addSql('ALTER TABLE historique ADD CONSTRAINT FK_EDBFD5EC7F1732D FOREIGN KEY (saisoon_id) REFERENCES saisoon (id)');
        $this->addSql('ALTER TABLE logo ADD CONSTRAINT FK_E48E9A1361190A32 FOREIGN KEY (club_id) REFERENCES club (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE historique DROP FOREIGN KEY FK_EDBFD5EC61190A32');
        $this->addSql('ALTER TABLE logo DROP FOREIGN KEY FK_E48E9A1361190A32');
        $this->addSql('ALTER TABLE historique DROP FOREIGN KEY FK_EDBFD5ECA9E2D76C');
        $this->addSql('ALTER TABLE historique DROP FOREIGN KEY FK_EDBFD5EC7F1732D');
        $this->addSql('DROP TABLE club');
        $this->addSql('DROP TABLE historique');
        $this->addSql('DROP TABLE joueur');
        $this->addSql('DROP TABLE logo');
        $this->addSql('DROP TABLE saisoon');
    }
}
